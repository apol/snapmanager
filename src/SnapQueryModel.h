/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SNAPQUERYMODEL_H
#define SNAPQUERYMODEL_H

#include <QAbstractListModel>

class QSnapdClient;
class QSnapdSnap;

class SnapQueryModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QSnapdClient* client READ client WRITE setClient NOTIFY clientChanged)
    Q_PROPERTY(Queries query READ query WRITE setQuery NOTIFY queryChanged)

public:
    enum Queries {
        List
    };
    Q_ENUM(Queries)

    SnapQueryModel(QObject* parent = nullptr);
    ~SnapQueryModel() override;

    QSnapdClient* client() const;
    void setClient(QSnapdClient* client);

    void setQuery(Queries queries);
    Queries query() const;

    virtual QVariant data(const QModelIndex& index, int role) const override;
    virtual int rowCount(const QModelIndex& parent) const override;

public Q_SLOTS:
    void refresh();

Q_SIGNALS:
    void clientChanged(QSnapdClient* client);
    void queryChanged(Queries query);

private:
    void setSnaps(const QVector<QSharedPointer<QSnapdSnap>>& snaps);
    QVector<QSharedPointer<QSnapdSnap>> m_snaps;

    Queries m_query = List;
    QSnapdClient* m_client = nullptr;
};

#endif // SNAPQUERYMODEL_H
