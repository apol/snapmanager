/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import org.kde.kirigami 2.0 as Kirigami
import org.kde.snapmanager 1.0

Kirigami.ApplicationWindow
{
    id: window
    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("Snap Manager")
        bannerImageSource: "https://snapforum.s3.amazonaws.com/original/1X/db7b8ce49da21ed543db125053e034dc61f784c8.png"

        actions: [
            Kirigami.Action {
                text: i18n("Installed")
                onTriggered: {
                    window.pageStack.clear()
                    window.pageStack.push("qrc:/qml/ApplicationListPage.qml", {title: i18n("Installed"), query: SnapQueryModel.List })
                }
            }
        ]
        Kirigami.Label {
            text: "state " + Snap.client
        }
    }

}
