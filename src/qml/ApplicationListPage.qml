/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5
import org.kde.kirigami 2.1 as Kirigami
import org.kde.snapmanager 1.0

Kirigami.ScrollablePage
{
    property alias query: snapQuery.query
    Component.onCompleted: snapQuery.refresh()
    ListView {
        headerPositioning: ListView.OverlayHeader
        header: Kirigami.ItemViewHeader {}

        model: SnapQueryModel {
            id: snapQuery
            client: Snap.client
        }
        delegate: Kirigami.BasicListItem {
            label: display
        }
    }
}
