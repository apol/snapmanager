/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "SnapQueryModel.h"
#include <Snapd/Client>

SnapQueryModel::SnapQueryModel(QObject* parent)
    : QAbstractListModel(parent)
{
}

SnapQueryModel::~SnapQueryModel()
{
}

QSnapdClient * SnapQueryModel::client() const
{
    return m_client;
}

void SnapQueryModel::setClient(QSnapdClient* client)
{
    if (client != m_client) {
        m_client = client;
        Q_EMIT clientChanged(client);
    }
}

SnapQueryModel::Queries SnapQueryModel::query() const
{
    return m_query;
}

void SnapQueryModel::setQuery(SnapQueryModel::Queries query)
{
    if (m_query != query) {
        m_query = query;
        Q_EMIT queryChanged(query);
    }
}

void SnapQueryModel::refresh()
{
    if (!m_client) {
        qWarning() << "cannot fetch without a client";
        return;
    }

    switch(m_query) {
        case List: {
            auto job = m_client->list();
            qDebug() << "listing";
            connect(job, &QSnapdListRequest::complete, this, [this, job]() {
                qDebug() << "list done";
                QVector<QSharedPointer<QSnapdSnap>> snaps;
                snaps.reserve(job->snapCount());
                for (int i=0, c=job->snapCount(); i<c; ++i) {
                    QSharedPointer<QSnapdSnap> snap(job->snap(i));
                    const auto snapname = snap->name();
                    snaps << snap;
                }
                setSnaps(snaps);
            });
            job->runSync();
            break;
        }
    }
}

void SnapQueryModel::setSnaps(const QVector<QSharedPointer<QSnapdSnap> >& snaps)
{
    beginResetModel();
    m_snaps = snaps;
    endResetModel();
}

QVariant SnapQueryModel::data(const QModelIndex& index, int role) const
{
    if (index.row() >= m_snaps.count())
        return {};

    auto snap = m_snaps[index.row()];
    switch(role) {
        case Qt::DisplayRole:
            return snap->name();
    }
    return {};
}

int SnapQueryModel::rowCount(const QModelIndex& parent) const
{
    return m_snaps.count();
}

