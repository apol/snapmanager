/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QCoreApplication>
#include <QLocalSocket>
#include <QTcpServer>
#include <QTcpSocket>

#define SNAPD_SOCKET "/run/snapd.socket"

class SnapConnection : public QObject
{
    Q_OBJECT

public:
    SnapConnection(QTcpSocket *clientConnection)
        : QObject(clientConnection)
        , m_client(clientConnection)
    {
        qDebug() << "new client" << clientConnection->peerAddress() << clientConnection->peerPort();
        connect(clientConnection, &QAbstractSocket::disconnected, clientConnection, &QObject::deleteLater);

        connect(&m_local, &QLocalSocket::connected, this, &SnapConnection::setup);
        m_local.connectToServer(SNAPD_SOCKET);
        QObject::connect(&m_local, static_cast<void(QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error), this, [this](QLocalSocket::LocalSocketError error) {
            qWarning() << "error:" << error << m_local.errorString();
        });
    }

    void setup()
    {
        qDebug() << "setup!";
        connect(&m_local, &QIODevice::readyRead, [this](){
            qDebug() << "read1!";
            const auto data = m_local.readAll();
            qDebug() << "read1" << data;
            m_client->write(data);
        });
        connect(m_client, &QIODevice::readyRead, [this](){
            qDebug() << "read2!";
            const auto data = m_client->readAll();
            qDebug() << "read2" << data;
            m_local.write(data);
        });
    }

private:
    QLocalSocket m_local;
    QTcpSocket* m_client;
};

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    QTcpServer tcpServer;
    QObject::connect(&tcpServer, &QTcpServer::newConnection, &app, [&tcpServer](){
        QTcpSocket *clientConnection = tcpServer.nextPendingConnection();
        qDebug() << "connection arrived";
        new SnapConnection(clientConnection);
    });
    if (!tcpServer.listen(QHostAddress::Any, 7627)) {
        qWarning() << "couldn't listen";
    }

    return app.exec();
}

#include "proxy.moc"
