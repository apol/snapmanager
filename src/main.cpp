/*
 *   Copyright (C) 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library/Lesser General Public License
 *   version 2, or (at your option) any later version, as published by the
 *   Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library/Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTcpSocket>
#include <QHostAddress>
#include <Snapd/Client>
#include <KLocalizedContext>
#include "SnapQueryModel.h"

class SnapSingleton : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QSnapdClient* client READ client NOTIFY clientChanged)
    public:
        SnapSingleton(QObject* parent)
            : QObject(parent)
        {
            m_socket.connectToHost(QHostAddress::LocalHost, 7627);
            qDebug() << "connecting...";
            QObject::connect(&m_socket, &QTcpSocket::stateChanged, this, [](QTcpSocket::SocketState state) {
                qDebug() << "state changed" << state;
            });
            QObject::connect(&m_socket, &QTcpSocket::connected, this, &SnapSingleton::connected);
            QObject::connect(&m_socket, static_cast<void(QTcpSocket::*)(QTcpSocket::SocketError)>(&QTcpSocket::error), this, [this](QTcpSocket::SocketError error) {
                qWarning() << "error:" << error << m_socket.errorString();
            });
        }

        void connected() {
            Q_ASSERT(!m_client);
            qDebug() << "connected" << m_socket.socketDescriptor();
            m_client.reset(new QSnapdClient(m_socket.socketDescriptor(), this));

            QScopedPointer<QSnapdRequest> request(m_client->connect());
            request->runSync();
            auto valid = request->error() == QSnapdRequest::NoError;
            if (!valid) {
                qWarning() << "snap problem at initialize:" << request->errorString();
                return;
            }
            Q_EMIT clientChanged(m_client.data());
        }

        QSnapdClient* client() {
            return m_client.data();
        }

    Q_SIGNALS:
        void clientChanged(QSnapdClient* client);

    private:
        QScopedPointer<QSnapdClient> m_client;
        QTcpSocket m_socket;
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    qmlRegisterType<QSnapdClient>();
    qmlRegisterType<SnapQueryModel>("org.kde.snapmanager", 1, 0, "SnapQueryModel");
    qmlRegisterSingletonType<SnapSingleton>("org.kde.snapmanager", 1, 0, "Snap", [](QQmlEngine* e, QJSEngine*) -> QObject* { return new SnapSingleton(e); });

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();
}

#include "main.moc"
